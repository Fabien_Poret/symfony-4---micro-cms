<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\ArticleType;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Request;

class AdministrationController extends AbstractController
{
    /**
     * @Route("/administration", name="administration")
     */
    public function index()
    {
        return $this->render('administration/index.html.twig', [
            'controller_name' => 'AdministrationController',
        ]);
    }

    /**
     * @Route("/administration/creationArticle", name="creationArticle")
     * @Route("/{id}/edit", name="blog_edit")
     */
    public function createArticle(Article $article = null, Request $request, ObjectManager $manager, ArticleRepository $repo)
    {
        if(!$article){
            $article = new Article(); 
        }

        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
 
        if($request->isMethod('POST')) {
            if($request->request->has('com_id')) {
                $id = $request->request->get('com_id');
                $action = ($request->request->has("delete")) ? 'delete' : 'edit';
        
                if($action === 'delete') {
                    $commentblog = $repo->findById($id);
                    $manager->remove($commentblog[0]);
                    $manager->flush();
                    return $this->redirectToRoute('blog_edit', ['id' => $article->getId()]);
                }
            }
        }

         if($form->isSubmitted() && $form->isValid()) {
            $article->setDateCreation(new \DateTime());
            $article->setAutheur("Fabien PORET");
            dump($article);
            $manager->persist($article);
            $manager->flush();
            return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);
         }

        // if($security->getUser()){ 
        //     $user = $security->getUser()->getUsername();
        //     $roleUser = $repoUser->findOneBy(['username' => $user]);
        //     $roleUser = $roleUser->role;
        // } else {
        //     $roleUser = "Non connecté";
        //     $user = "Non connecté";
        // }


        return $this->render('administration/createArticle.html.twig', [
            'controller_name' => 'AdministrationController',
            'article' => $article,
            'editMode' => $article->getId() !== null,
            'formArticle' =>$form->createView()
        ]);
    }

    /**
     * @Route("/administration/showArticle", name="adminArticle")
     * 
     */
    public function adminArticle(ArticleRepository $repo, Request $request, ObjectManager $manager)
    {
        // if($security->getUser()){ 
        //     $user = $security->getUser()->getUsername();
        //     $roleUser = $repoUser->findOneBy(['username' => $user]);
        //     $roleUser = $roleUser->role;
        // }
        // else {
        //     $roleUser = "Non connecté";
        //     $user = "Non connecté";
        // }
        $articles = $repo->findAll();
        $articles = array_reverse($articles);

        if($request->isMethod('POST')) {
            if($request->request->has('blog_id')) {
                $idArticle = $request->request->get('blog_id');
                $actionArticle = ($request->request->has("deleteArticle")) ? 'deleteArticle' : 'editArticle';
                if($actionArticle === 'deleteArticle') {
                    $article = $repo->findById($idArticle);
                    $manager->remove($article[0]);
                    $manager->flush();
                    return $this->redirectToRoute('adminArticle');
                }
            }
        }

        return $this->render('administration/adminArticle.html.twig', [
            'controller_name' => 'AdminController',
            // 'roleUser' => $roleUser,
            
            'articles' => $articles,
            // 'user' => $user
        ]);
    }

    
    /**
     * @Route("/administration/createTemplate", name="createTemplate")
     * 
     */
    public function createTemplate()
    {
        
        return $this->render('administration/createTemplate.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * @Route("/administration/seo", name="seo")
     * 
     */
    public function updateSeo()
    {
        //TODO: Changer le lien à la mise en ligne
        $dir    = '/Dev/Master/microCMS/microCMS/templates/';
        $files1 = scandir($dir);

        $docTemplate = [];
        $pages = [];
        $lostDoc = [];
        $dirDoc = [];
        // $tmp = [];
        foreach ($files1 as $value) {
            $strlenDir = strlen($dir);
            $strlenDir = $strlenDir + 2;
            $dir = '/Dev/Master/microCMS/microCMS/templates/%s/';
            $fullDir = sprintf($dir, $value);
            if(is_dir($fullDir)){
                if(strlen($fullDir) > $strlenDir){
                    array_push($docTemplate, $fullDir);

                    $dirPages = scandir($fullDir);
    
                    foreach($dirPages as $tmpDir){
                        if(is_dir($tmpDir)){
                            array_push($docTemplate, $tmpDir);
                        }
                        else{
                            array_push($pages, $tmpDir);
                        }
                    }
                }
                else{
                    array_push($lostDoc, $fullDir);
                }
            }
            else{
                // array_push($pages, $fullDir);
            }
        }

        return $this->render('administration/seo.html.twig', [
            'controller_name' => 'AdminController',
            'pages' => $pages,
        ]);
    }
}
