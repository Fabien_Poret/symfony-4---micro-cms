<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Article;

class ArticlesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');
        // $product = new Product();
    
        $content = join($faker->paragraphs(10));
      
        for($i = 1; $i <= 15; $i++) {
            $article = new Article();
            $article->setTitre($faker->sentence())
                    ->setContenu($content)
                    ->setImage($faker->imageUrl)
                    ->setDateCreation($faker->dateTimeBetween('-6 months'))
                    ->setAutheur($faker->name)
                    ->setDateModification($faker->dateTimeBetween('-1 months'))
                    ->setCategorie($faker->name);
            $manager->persist($article);
        }
  
        $manager->flush();
    }
}
