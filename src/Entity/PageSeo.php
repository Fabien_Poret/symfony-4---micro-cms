<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PageSeoRepository")
 */
class PageSeo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_page;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pageTitle;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $pageDesc;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdPage(): ?int
    {
        return $this->id_page;
    }

    public function setIdPage(int $id_page): self
    {
        $this->id_page = $id_page;

        return $this;
    }

    public function getPageTitle(): ?string
    {
        return $this->pageTitle;
    }

    public function setPageTitle(?string $pageTitle): self
    {
        $this->pageTitle = $pageTitle;

        return $this;
    }

    public function getPageDesc(): ?string
    {
        return $this->pageDesc;
    }

    public function setPageDesc(?string $pageDesc): self
    {
        $this->pageDesc = $pageDesc;

        return $this;
    }
}
